=== WooCommerce Facebook Login ===
Contributors: zpellman
Tags: woocommerce, facebook
Requires at least: 3.0
Tested up to: 3.5.1
Stable tag: 1.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Allows users to login via Facebook and pre-populates WooCommerce checkout fields with their Facebook account data.

== Description ==

XYZ allows customers to login via Facebook and then pre-populates the WooCommerce checkout page with information from their Facebook account. While many plugins exist that allow a user to login with their WordPress account, many of them don't work (or don't work well), and none of them integrate with WooCommerce.

== Installation ==

1. Upload `plugin-name.php` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Enter your Facebook App ID and App Secret on the settings page

== Changelog ==

= 1.2 =
* Implemented an alternative way of overriding the output from WooCommerce's woocommerce_login_form() function
* Fixed issue with populating first name in billing form

= 1.1 =
* Removed a function array dereference that required PHP 5.4 to make the plugin compatible with older versions of PHP

= 1.0 =
* Released
