<div class="error">
	<?php foreach ( $errors as $error ): ?>
		<p>
			<strong><?php _e( 'ERROR:', 'wfi' ); ?></strong>
			<?php echo $error; ?>
		</p>
	<?php endforeach; ?>
</div>