<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $woocommerce;

if (is_user_logged_in()) return;

$wfl_fb_app_id = get_option( 'wfl_fb_app_id' );

?>
<form method="post" class="login" <?php if ( $hidden ) echo 'style="display:none;"'; ?>>
    <?php if ( $message ) echo wpautop( wptexturize( $message ) ); ?>

    <p class="form-row form-row-first">
        <label for="username"><?php _e( 'Username or email', 'woocommerce' ); ?> <span class="required">*</span></label>
        <input type="text" class="input-text" name="username" id="username" />
    </p>
    <p class="form-row form-row-last">
        <label for="password"><?php _e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
        <input class="input-text" type="password" name="password" id="password" />
    </p>
    <div class="clear"></div>

    <p class="form-row">
        <?php $woocommerce->nonce_field('login', 'login') ?>
        <input type="submit" class="button" name="login" value="<?php _e( 'Login', 'woocommerce' ); ?>" />
        <input type="hidden" name="redirect" value="<?php echo esc_url( $redirect ) ?>" />
        <a class="lost_password" href="<?php echo esc_url( wp_lostpassword_url( home_url() ) ); ?>"><?php _e( 'Lost Password?', 'woocommerce' ); ?></a>
    </p>

    <?php if ( ! empty( $wfl_fb_app_id ) ): ?>
        <p class="form-row"><?php _e( 'Alternatively, you can login using your Facebook account', 'wfl' ); ?></p>

        <p class="form-row">
            <a href="javascript:void(0);"
                class="wfl-button"
                title="Login with Facebook"
                onclick="WFL.login('<?php echo $wfl_fb_app_id; ?>', '<?php echo site_url(); ?>')"
            >
                <?php _e( 'Login with Facebook', 'wfl' ); ?>
            </a>
        </p>
    <?php endif; ?>

    <div class="clear"></div>
</form>
