<div class="wrap">
	<div class="icon32 icon32-wfl-settings"></div>
	<h2><?php _e( 'WooCommerce Facebook Login', 'wfl' ); ?></h2>

	<form name="woocommerce-facebook-login-settings" method="post" action="">
		<table class="form-table wfl-table">
			<thead>
				<tr>
					<th colspan="2" class="wfl-table-header"><?php _e( 'Facebook App Settings', 'wfl' ); ?></th>
				</tr>
			</thead>
			<tr class="wfl-table-row wfl-table-row-odd">
				<th class="wfl-row-header" scope="row">
					<label for="fb-app-id">
						<?php _e( 'Facebook App ID', 'wfl' ); ?>
					</label>
				</th>
				<td>
					<input type="text"
						name="fb-app-id"
						class="regular-text"
						id="fb-app-id"
						value="<?php esc_html_e( $fb_app_id ); ?>"
					>
				</td>
			</tr>
			<tr class="wfl-table-row wfl-table-row-even">
				<th class="wfl-row-header" scope="row">
					<label for="fb-app-secret">
						<?php _e( 'Facebook App Secret', 'wfl' ); ?>
					</label>
				</th>
				<td>
					<input type="text"
						name="fb-app-secret"
						class="regular-text"
						id="fb-app-secret"
						value="<?php esc_html_e( $fb_app_secret ); ?>"
					>
				</td>
			</tr>
		</table>

		<p class="submit">
			<input type="submit" name="submit" class="button button-primary" value="<?php esc_attr_e( 'Save Changes' ); ?>" />
		</p>
	</form>
</div>