var WFL = {
	login: function( appId, redirectUrl ) {
		// make sure both params are provided
		if ( undefined === appId || undefined === redirectUrl ) {
			return false;
		}

		// checks that the plugin has been configured
		if ( '' === appId ) {
			alert( 'The Facebook app settings for this site have not yet been configured. Please contact the website owner.' );
			return false;
		}

		// opens the Facebook login URL
		window.open(
			'https://graph.facebook.com/oauth/authorize?client_id=' + appId + '&redirect_uri=' + redirectUrl + '&scope=email,user_about_me',
			'',
			'scrollbars=no,menubar=no,height=400,width=800,resizable=yes,toolbar=no,status=no'
		);
	},

	/**
	 * Creates a form to redirect the user with the Facebook access token via $_POST
	 *
	 * @param string action        The form action
	 * @param string fbAccessToken A Facebook access token
	 */
	redirect: function( action, fbAccessToken ) {
		var input, form;

		// creates the form element
		form = document.createElement( 'form' );

		form.action = action;
		form.method = 'post';
		form.style.display = 'none';

		// creates the input element containing the access token
		input = document.createElement( 'input' );

		input.name  = 'access_token';
		input.type  = 'hidden';
		input.value = fbAccessToken;

		// adds the input element to the form element
		form.appendChild( input );

		// adds the form to the HTML DOM
		document.body.appendChild( form );

		// submits the form
		form.submit();
	},
};