<?php
/**
 * Plugin Name: WooCommerce Facebook Login
 * Plugin URI: https://bitbucket.org/zpellman/woocommerce-facebook-login
 * Description: Allows users to login via Facebook and pre-populates WooCommerce checkout fields with their Facebook account data.
 * Version: 1.2
 * Author: Zach Pellman <zpellman@gmail.com>
 * Author URI: http://www.zachpellman.com
 * License: GPL2
 *
 * Copyright 2013 Zach Pellman <zpellman@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 2, as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

/*
 * Registers action and filter hooks
 */
add_action( 'admin_init', 'wfl_register_admin_styles' );
add_action( 'admin_menu', 'wfl_add_settings_page' );
add_action( 'init',       'wfl_process_user_login' );
add_action( 'init',       'wfl_register_frontend_styles' );
add_action( 'login_form', 'wfl_render_facebook_login_form' );
add_action( 'login_head', 'wfl_enqueue_frontend_scripts' );
add_action( 'login_head', 'wfl_enqueue_frontend_styles' );
add_action( 'wp_head',    'wfl_enqueue_frontend_scripts' );
add_action( 'wp_head',    'wfl_enqueue_frontend_styles' );

add_filter( 'default_checkout_state', 'wfl_populate_checkout_state' );
add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'wfl_add_action_links' );
add_filter( 'woocommerce_checkout_fields', 'wfl_populate_checkout_fields' );
add_filter( 'woocommerce_locate_template', 'wfl_woocommerce_locate_template' );

/**
 * Adds a "Settings" link to the Plugins page
 *
 * @param mixed $links The current action links
 */
function wfl_add_action_links( $links ) {
    // gets the link to add
    $settings_link = sprintf( '<a href="%s">%s</a>', 'admin.php?page=' . __FILE__, __( 'Settings', 'wfl' ) );

    // prepends the link to $links
    array_unshift( $links, $settings_link );

    return $links;
}

/**
 * Adds a sub-menu to the WooCommerce admin menu
 */
function wfl_add_settings_page() {
    // adds the sub-menu page
    $page = add_submenu_page(
        'woocommerce',
        __( 'WooCommerce Facebook Login', 'wfl' ),
        __( 'Facebook Login', 'wfl' ),
        'manage_options',
        __FILE__,
        'wfl_display_settings_page'
    );

    // enqueues the admin stylesheet
    add_action( 'admin_print_styles-' . $page, 'wfl_enqueue_admin_styles' );
}

/**
 * Gets a unique username
 *
 * This function checks to see $user_login is already a registered username,
 * and if it is, appends an incrementing number until it is unique
 *
 * @param string $user_login The username to create
 *
 * @return string A unique username
 */
function wfl_create_unique_username( $user_login ) {
    $i = 0;

    /*
     * you would expect a function called "username_exists" to return true/false,
     * but it returns a user ID or null if it doesn't exist instead
     */
    while ( null !== username_exists( $user_login ) ) {
        // increments $i than appends it
        $user_login .= ++$i;
    }

    return $user_login;
}

/**
 * Displays the settings page
 */
function wfl_display_settings_page() {
    // checks that the user has the required capability
    if ( ! current_user_can( 'manage_options' ) ) {
        wp_die( __( 'You do not have sufficient permissions to access this page' ) );
    }

    // read the existing value from the database
    $fb_app_id     = get_option( 'wfl_fb_app_id' );
    $fb_app_secret = get_option( 'wfl_fb_app_secret' );

    // checks if the user has updated the settings
    if ( isset( $_POST['submit'] ) ) {
        // gets the new values for fb_app_id and fb_app_secret
        $fb_app_id     = $_POST['fb-app-id'];
        $fb_app_secret = $_POST['fb-app-secret'];

        // initializes an array of validation errors
        $errors = array();

        // validates fb_app_id and fb_app_secret
        if ( empty( $fb_app_id ) ) {
            $errors[] = __( 'You must provide your Facebook App ID', 'wfl' );
        }

        if ( empty( $fb_app_secret ) ) {
            $errors[] = __( 'You must provide your Facebook App Secret', 'wfl' );
        }

        /*
         * if there are no errors, save the values of
         * fb_app_id and fb_app_secret to the database
         */
        if ( 0 === count( $errors ) ) {
            update_option( 'wfl_fb_app_id',     $fb_app_id );
            update_option( 'wfl_fb_app_secret', $fb_app_secret );

            // displays a success message
            require_once( 'tmpl/settings-success.php' );
        } else {
            // displays a success message
            require_once( 'tmpl/settings-error.php' );
        }
    }

    // displays the settings page
    require_once( 'tmpl/settings-page.php' );
}

/**
 * Enqueues the settings page styles
 */
function wfl_enqueue_admin_styles() {
    wp_enqueue_style( 'wfl_admin_styles' );
}

/**
 * Enqueues the front-end scripts
 */
function wfl_enqueue_frontend_scripts() {
    wp_enqueue_script( 'wfl_frontend_script', plugins_url( 'assets/js/wfl-frontend.js', __FILE__ ) );
}

/**
 * Enqueues the front-end styles
 */
function wfl_enqueue_frontend_styles() {
    wp_enqueue_style( 'wfl_frontend_styles' );
}

/**
 * Gets a WordPress user ID by their Facebook account ID
 *
 * This function checks if a user exists in the database with a 'wfl_fb_user_id'
 * meta key matching the provided Facebook account ID
 *
 * @param string $fb_user_id The meta value to search for
 *
 * @return mixed User ID or false if no user was found
 */
function wfl_get_user_by_facebook_id( $fb_user_id ) {
    $user_id = get_users( array(
        'meta_key' => 'wfl_fb_user_id',
        'meta_value' => $fb_user_id,
        'fields' => 'ID',
    ) );

    if ( empty( $user_id ) ) {
        return false;
    }

    return $user_id[0];
}

/**
 * Populates the WooCommerce "state" field in the checkout form
 *
 * @param mixed $state The state to filter
 *
 * @return mixed The filtered state
 */
function wfl_populate_checkout_state( $state ) {
    // gets the user's ID
    $user_id = get_current_user_id();

    if ( 0 === $user_id ) {
        // stop here. The user is not logged in
        return $state;
    }

    // get the user's state from their Facebook location stored as meta data
    $location = explode( ', ', get_user_meta( $user_id, 'wfl_fb_user_location', true ) );

    // converts the full state name into its abbreviation
    $states_abbrs = array(
        'Alabama' => 'AL', 'Alaska' => 'AK', 'Arizona' => 'AZ', 'Arkansas' => 'AR',
        'California' => 'CA', 'Colorado' => 'CO', 'Connecticut' => 'CT', 'Delaware' => 'DE',
        'District of Columbia' => 'DC', 'Florida' => 'FL', 'Georgia' => 'GA', 'Hawaii' => 'HI',
        'Idaho' => 'ID', 'Illinois' => 'IL', 'Indiana' => 'IN', 'Iowa' => 'IA',
        'Kansas' => 'KS', 'Kentucky' => 'KY', 'Louisiana' => 'LA', 'Maine' => 'ME',
        'Maryland' => 'MD', 'Massachusetts' => 'MA', 'Michigan' => 'MI', 'Minnesota' => 'MN',
        'Mississippi' => 'MS', 'Missouri' => 'MO', 'Montana' => 'MT', 'Nebraska' => 'NE',
        'Nevada' => 'NV', 'New Hampshire' => 'NH', 'New Jersey' => 'NJ', 'New Mexico' => 'NM',
        'New York' => 'NY', 'North Carolina' => 'NC', 'North Dakota' => 'ND', 'Ohio' => 'OH',
        'Oklahoma' => 'OK', 'Oregon' => 'OR', 'Pennsylvania' => 'PA', 'Rhode Island' => 'RI',
        'South Carolina' => 'SC', 'South Dakota' => 'SD', 'Tennessee' => 'TN', 'Texas' => 'TX',
        'Utah' => 'UT', 'Vermont' => 'VT', 'Virginia' => 'VA', 'Washington' => 'WA',
        'West Virginia' => 'WV', 'Wisconsin' => 'WI', 'Wyoming' => 'WY', 'American Samoa' => 'AS',
        'Guam' => 'GU', 'Northern Mariana Islands' => 'MP', 'Puerto Rico' => 'PR',
        'Virgin Islands' => 'VI', 'U.S. Minor Outlying Islands' => 'UM',
        'Federated States of Micronesia' => 'FM', 'Marshall Islands' => 'MH', 'Palau' => 'PW',
    );

    /*
     * Note: I chose to iterate over the array versus using $location as the array index
     * just in case Facebook returned a location that wasn't in city, state format or the
     * state was spelled/capitalized differently than in the array and I didn't want
     * any undefined index errors
     */
     foreach ( $states_abbrs as $state => $abbr ) {
        if ( strtolower($location[1]) === strtolower($state) ) {
            return $abbr;
        }
    }

    return $state;
}

/**
 * Populates the WooCommerce checkout fields. Note the state field is done separately
 *
 * @param mixed $fields The checkout fields to filter
 *
 * @return mixed The filtered fields
 */
function wfl_populate_checkout_fields( $fields ) {
    // gets the user's ID
    $user_id = get_current_user_id();

    if ( 0 === $user_id ) {
        // stop here. The user is not logged in
        return $fields;
    }

    // get the user's Facebook location stored as meta data
    $user_meta = get_user_meta( $user_id );

    $first_name = $user_meta['first_name'][0];
    $last_name = $user_meta['last_name'][0];
    $email = $user_meta['wfl_fb_user_email'][0];
    $location = explode( ', ', $user_meta['wfl_fb_user_location'][0] );

    // pre-populates the billing fields
    $fields['billing']['billing_first_name']['default'] = $first_name;
    $fields['billing']['billing_last_name']['default']  = $last_name;
    $fields['billing']['billing_city']['default']       = $location[0];
    $fields['billing']['billing_email']['default']      = $email;

    // pre-populates the shipping fields
    $fields['shipping']['shipping_first_name']['default'] = $first_name;
    $fields['shipping']['shipping_last_name']['default']  = $last_name;
    $fields['shipping']['shipping_city']['default']       = $location[0];
    $fields['shipping']['shipping_email']['default']      = $email;

    return $fields;
}

/**
 * Processes a user login
 */
function wfl_process_user_login() {
    // gets the app ID and secret from the database
    $fb_app_id     = get_option( 'wfl_fb_app_id' );
    $fb_app_secret = get_option( 'wfl_fb_app_secret' );

    /*
     * Checks if the user is logged into Facebook ('code' is present)
     * and exchanges the code for an access token
     */
    if ( isset( $_GET['code'] ) && ! empty( $_GET['code'] ) ) {
        // builds the query string
        $query_data = array(
            'client_id' => $fb_app_id,
            'redirect_uri' => site_url( '/' ), // trailing slash is required or else FB will throw error
            'client_secret' => $fb_app_secret,
            'code' => $_GET['code'],
        );

        // creates the request url
        $url = 'https://graph.facebook.com/oauth/access_token?' . http_build_query( $query_data );

        // requests an access token for the user
        $response = wfl_fb_api_call( $url );

        // parses the response string. The access token will be stored as $access_token
        parse_str( $response );

        /*
         * The script below causes the window that opened the Facebook login
         * window to redirect with the access token as a query parameter and
         * then closes the pop-up. Redirecting the parent window is necessary
         * because without it, there is nothing to indicate to the user that
         * the login was successfull
         */
        // TODO: need to redirect in parent and then test for access_token in query string below
        ?>
        <script>
            window.opener.WFL.redirect( '<?php echo home_url(); ?>', '<?php echo $access_token; ?>' );
            window.close();
        </script>
        <?php
    }

    if ( isset( $_POST['access_token'] ) ) {
        $access_token = $_POST['access_token'];

        $url = 'https://graph.facebook.com/me?access_token=' . $access_token;

        // gets the user's Facebook account information
        $fb_user = json_decode( wfl_fb_api_call( $url ) );

        // checks that a valid Facebook account was returned
        if ( ! empty( $fb_user->id ) && ! empty( $fb_user->email ) ) {
            // checks if a user exists with this Facebook account ID
            $user_id = wfl_get_user_by_facebook_id( $fb_user->id );

            // no user exists that has this Facebook account linked to their WordPress account
            if ( false === $user_id ) {
                // a user exists whose WordPress account email matches this Facebook account's email. Links the accounts
                if ( false !== ($user_id = email_exists( $fb_user->email ) ) ) {
                    // gets the user's user data
                    $user_data = get_userdata( $user_id );

                    if ( false !== $user_data ) {
                        // makes sure that no other users have this Facebook account ID linked to them
                        delete_user_meta( $user_id, 'wfl_fb_user_id', $fb_user->id );

                        // adds the user's Facebook account ID and email to their WordPress account user meta
                        update_user_meta( $user_id, 'wfl_fb_user_id', $fb_user->id );
                        update_user_meta( $user_id, 'wfl_fb_user_email', $fb_user->email );
                        update_user_meta( $user_id, 'wfl_fb_user_location', $fb_user->location->name );

                        /*
                         * ensures that the user's Facebook data is cached instead of
                         * any previous WordPress data for the remainder of this request
                         */
                        wp_cache_delete( $user_id, 'users' );
                        wp_cache_delete( $user_data->user_login, 'userlogins' );
                    }
                }

                // no user exists whose WordPress account email matches this Facebook account's email. Create an account
                else {
                    // creates the display name and a unique user login for the new user
                    $email_parts = explode( '@', $fb_user->email );
                    $display_name = $user_login = $email_parts[0];

                    if ( ! empty( $fb_user->name ) ) {
                        $display_name = $user_login = $fb_user->name;
                    } else if ( ! empty( $fb_user->first_name ) && ! empty( $fb_user->last_name ) ) {
                        $display_name = $fb_user->first_name . ' ' . $fb_user->last_name;
                        $user_login = $fb_user->first_name . $fb_user->last_name;
                    }

                    $user_login = wfl_create_unique_username( sanitize_user( $user_login ) );

                    // generates a password for the new user
                    $user_password = wp_generate_password();

                    // gets the default user role
                    $user_role = get_option( 'default_role' );

                    // sets the user data for the new account
                    $user_data = array(
                        'user_login' => $user_login,
                        'display_name' => (!empty ($fbdata['name']) ? $fbdata['name'] : $user_login),
                        'user_email' => $fb_user->email,
                        'first_name' => $fb_user->first_name,
                        'last_name' => $fb_user->last_name,
                        'user_pass' => $user_password,
                        'role' => $user_role
                    );

                    // create the user's WordPress acount
                    $user_id = wp_insert_user( $user_data );

                    if ( ! is_wp_error( $user_id ) ) {
                        // makes sure that no other users have this Facebook account ID linked to them
                        delete_user_meta( $user_id, 'wfl_fb_user_id', $fb_user->id );

                        // adds the user's Facebook account ID, email, and location to their WordPress account user meta
                        update_user_meta( $user_id, 'wfl_fb_user_id', $fb_user->id );
                        update_user_meta( $user_id, 'wfl_fb_user_email', $fb_user->email );
                        update_user_meta( $user_id, 'wfl_fb_user_location', $fb_user->location->name );

                        /*
                         * ensures that the user's Facebook data is cached instead of
                         * any previous WordPress data for the remainder of this request
                         */
                        wp_cache_delete( $user_id, 'users' );
                        wp_cache_delete( $user_data->user_login, 'userlogins' );

                        // user_register hooks
                        do_action ('user_register', $user_id);
                    }
                }
            }

            /*
             * A new account has been created, a Facebook account has been linked
             * to an existing account, or the user has successfully logged in
             */
            $user_data = get_userdata( $user_id );

            if ( false !== $user_data ) {
                // reset cookies
                wp_clear_auth_cookie();
                wp_set_auth_cookie( $user_data->ID, true );

                // call wp_login hooks
                do_action( 'wp_login', $user_data->user_login, $user_data );

                // redirect
                wp_redirect( home_url() );
            }

            // WordPress recommends exiting after wp_redirect
            exit;
        }
    }
}

/**
 * Registers the settings page styles
 */
function wfl_register_admin_styles() {
    wp_register_style( 'wfl_admin_styles', plugins_url( 'assets/css/wfl-admin.css', __FILE__ ) );
}

/**
 * Registers the front-end styles
 */
function wfl_register_frontend_styles() {
    wp_register_style( 'wfl_frontend_styles', plugins_url( 'assets/css/wfl-frontend.css', __FILE__ ) );
}

/**
 * Renders the Facebook login form
 */
function wfl_render_facebook_login_form() {
    /* TODO: Not sure if I want to keep this or not
    if ( is_user_logged_in() ) {
        return false;
    }
    */

    // gets the new values for fb_app_id and fb_app_secret
    $fb_app_id = get_option( 'wfl_fb_app_id' );

    require_once( 'tmpl/fb-login-form.php' );
}

/**
 * Makes a call to the Facebook Graph API using cURL
 */
function wfl_fb_api_call( $url ) {
    $ch = curl_init();

    curl_setopt( $ch, CURLOPT_URL, $url );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );

    $response = curl_exec( $ch );

    curl_close( $ch );

    return $response;
}

/**
 * Overrides the woocommerce_login_form() function to add the Facebook
 * login button too the WooCommerce login form
 */

function wfl_woocommerce_locate_template( $template, $template_name, $template_path ) {
    $wfl_login_form_path = dirname( __FILE__ ) . '/tmpl/woocommerce-login.php';
    $woo_login_form_path = ABSPATH . 'wp-content/plugins/woocommerce/templates/shop/form-login.php';

    if ($template === $woo_login_form_path) {
        return $wfl_login_form_path;
    }

    return $template;
}
